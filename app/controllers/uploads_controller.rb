class UploadsController < ApplicationController
	before_action :authenticate_admin
	def index
		@uploads = Upload.order("created_at DESC").includes(:user_agent).page(params[:page]).per(100).without_count
	end
end
