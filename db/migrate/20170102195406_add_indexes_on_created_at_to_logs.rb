class AddIndexesOnCreatedAtToLogs < ActiveRecord::Migration[4.2]
  def change
	add_index :downloads, :created_at
	add_index :thaw_requests, :created_at
	add_index :file_freezes, :created_at
  end
end
