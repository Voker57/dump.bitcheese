class AddIndexOnHash < ActiveRecord::Migration[4.2]
  def change
		add_index :dumped_files, :file_hash
  end
end
